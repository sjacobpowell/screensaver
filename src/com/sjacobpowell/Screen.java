package com.sjacobpowell;

import java.util.Random;

public class Screen extends Bitmap {
	private Bitmap background;
	private Random random;

	public Screen(int width, int height) {
		super(width, height);
		background = new Bitmap(width, height);
		random = new Random();
	}

	public void render(ScreenSaver screenSaver) {
		clear();

		for (int i = 0; i < background.pixels.length; i++) {
			background.pixels[i] = random.nextInt(0xffffff);
		}
		draw(background, 0, 0);

		Entity block = screenSaver.block;
		int pixel;
		for (int i = 0; i < block.sprite.pixels.length; i++) {
			pixel = block.sprite.pixels[i];
			if (pixel < 0x0000ff) {
				block.sprite.pixels[i] += 0x000003;
			} else if (pixel < 0x00ff00) {
				block.sprite.pixels[i] += 0x000300;
				block.sprite.pixels[i] -= 0x000003;
			} else if(pixel < 0xff0000){
				block.sprite.pixels[i] += 0x030000;
				block.sprite.pixels[i] -= 0x000300;
			} else {
				block.sprite.pixels[i] = 0x000000;
			}
		}
		draw(ArtTools.circlefy(block.sprite, block.sprite.width / 2.2, 0, 0), block.getXInt(), block.getYInt());
	}
}
