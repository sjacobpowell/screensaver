package com.sjacobpowell;

public class ScreenSaver {
	public int time;
	public Entity block;
	private int width;
	private int height;

	public ScreenSaver(int width, int height) {
		this.width = width;
		this.height = height;
		block = new Entity();
		block.sprite = new Bitmap(64, 64);
		block.xSpeed = 20;
		block.ySpeed = 20;
	}

	public void tick() {
		block.move();
		if(block.x <= 0) block.xSpeed = -block.xSpeed;
		if(block.y <= 0) block.ySpeed = -block.ySpeed;
		if(block.x >= width - block.sprite.width) block.xSpeed = -block.xSpeed;
		if(block.y >= height - block.sprite.height) block.ySpeed = -block.ySpeed;
		time++;
	}
}
