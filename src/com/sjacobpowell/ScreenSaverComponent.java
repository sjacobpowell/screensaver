package com.sjacobpowell;

import java.awt.BorderLayout;
import java.awt.Canvas;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class ScreenSaverComponent extends Canvas implements Runnable {
	private static final long serialVersionUID = 1L;
	public static int WIDTH = 320;
	public static int HEIGHT = 200;
	public static final int SCALE = 1;
	private boolean running = false;
	private Thread thread;
	private ScreenSaver screensaver;
	private Screen screen;
	private InputManager inputManager;
	private BufferedImage image;
	private int[] pixels;

	public ScreenSaverComponent() {
		GraphicsDevice device = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
		WIDTH = device.getDisplayMode().getWidth() * 2;
		HEIGHT = device.getDisplayMode().getHeight();
		Dimension size = new Dimension(WIDTH, HEIGHT);
		setSize(size);
		setPreferredSize(size);
		setMinimumSize(size);
		setMaximumSize(size);

		screensaver = new ScreenSaver(WIDTH, HEIGHT);
		screen = new Screen(WIDTH, HEIGHT);

		image = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_RGB);
		pixels = ((DataBufferInt) image.getRaster().getDataBuffer()).getData();

		inputManager = new InputManager();
		addKeyListener(inputManager);
		addMouseMotionListener(inputManager);
		setCursor(Toolkit.getDefaultToolkit().createCustomCursor(new BufferedImage(16, 16, BufferedImage.TYPE_INT_ARGB), new Point(0, 0), "empty"));
	}

	@Override
	public void run() {
		int frames = 0;
		int tickCount = 0;
		double FPS = 25.0;
		double unprocessedSeconds = 0;
		double secondsPerTick = 1 / FPS;
		double end = System.nanoTime();

		requestFocus();

		while (running) {
			double start = System.nanoTime();
			double passedTime = start - end;
			end = start;
			if (passedTime < 0)
				passedTime = 0;
			if (passedTime > 100000000)
				passedTime = 100000000;

			unprocessedSeconds += passedTime / 1000000000;

			boolean ticked = false;
			while (unprocessedSeconds > secondsPerTick) {
				tick();
				unprocessedSeconds -= secondsPerTick;
				ticked = true;

				tickCount++;
				if (tickCount % FPS == 0) {
					System.out.println(frames + " fps");
					end += 1000;
					frames = 0;
				}
			}

			if (ticked) {
				render();
				frames++;
			} else {
				try {
					Thread.sleep(1);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}

	private void tick() {
		screensaver.tick();
	}

	private void render() {
		BufferStrategy strategy = getBufferStrategy();
		if (strategy == null) {
			createBufferStrategy(3);
			return;
		}

		screen.render(screensaver);
		for (int i = 0; i < pixels.length; i++) {
			pixels[i] = screen.pixels[i];
		}

		Graphics g = strategy.getDrawGraphics();
		g.drawImage(image, 0, 0, WIDTH * SCALE, HEIGHT * SCALE, null);
		g.dispose();
		strategy.show();
	}

	public void start() {
		if (running)
			return;
		running = true;
		thread = new Thread(this);
		thread.start();
	}

	public void stop() {
		if (!running)
			return;
		running = false;
		try {
			thread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
        JFrame frame = new JFrame();
        frame.setUndecorated(true);
        frame.setResizable(false);
		ScreenSaverComponent screenSaver = new ScreenSaverComponent();
		JPanel panel = new JPanel(new BorderLayout());
		panel.add(screenSaver, BorderLayout.CENTER);
		frame.setContentPane(panel);
        frame.validate();
		frame.pack();
		frame.setLocation(0, 0);
		frame.setVisible(true);
		screenSaver.start();
	}
}
