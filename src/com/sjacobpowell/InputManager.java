package com.sjacobpowell;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

public class InputManager implements KeyListener, MouseListener, MouseMotionListener {

	@Override
	public void keyPressed(KeyEvent e) {
		System.exit(0);
	}

	@Override
	public void keyReleased(KeyEvent e) {
		System.exit(0);
	}

	@Override
	public void keyTyped(KeyEvent e) {
		System.exit(0);
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		System.exit(0);
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		System.exit(0);
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		System.exit(0);
	}

	@Override
	public void mousePressed(MouseEvent e) {
		System.exit(0);
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		System.exit(0);
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		System.exit(0);
	}

	@Override
	public void mouseExited(MouseEvent e) {
		System.exit(0);
	}

}
